<?php
return array(
	'type'=>'complex',
	'referer'=> 'https://www.damailicai.com/reg/user.html',
	'cookie'=> 'damailicai.cookie',
	'match'=> array(
		'url'=>'https://www.damailicai.com/reg/user.html',
		'data'=> '',
		'attributes'=> array('code'=>"#DAMAI_FTOKEN_NAME\"\s+value\=\"(\w+)\"#"),
		'ssl_verifypeer'=> true,
	),
	'list'=> array(
		array(
			'url'=>'https://www.damailicai.com/reg/user/reg_mobile_code_rand_roobt',
			'data'=>'checkcode={$code}&mobile={$mobile}',
			'post'=>true,
		),
	)
);